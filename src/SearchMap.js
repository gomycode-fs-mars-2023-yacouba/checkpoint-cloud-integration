import React, { useState } from 'react';
import Geocode from 'react-geocode';

// Set the geocoding API key (obtain it from the Google Cloud Console)
Geocode.setApiKey('AIzaSyDa_zPHwlYahYgDljMoTO01DzQGS8OxiGM');

function SearchMap() {
  const [searchQuery, setSearchQuery] = useState('');

  const handleSearch = async () => {
    try {
      const response = await Geocode.fromAddress(searchQuery);
      const { lat, lng } = response.results[0].geometry.location;
      console.log('Latitude:', lat);
      console.log('Longitude:', lng);
    } catch (error) {
      console.error('Error while searching for location:', error);
    }
  };

  return (
    <div>
      <input
        type="text"
        value={searchQuery}
        onChange={(e) => setSearchQuery(e.target.value)}
      />
      <button onClick={handleSearch}>Search</button>
    </div>
  );
}

export default SearchMap;
