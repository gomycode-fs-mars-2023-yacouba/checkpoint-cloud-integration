import React, { useState } from 'react';
import { GoogleMap, Marker, InfoWindow } from '@react-google-maps/api';
import './Map.css'; // Import du fichier CSS

const containerStyle = {
  width: '400px',
  height: '400px'
};

const center = {
  lat: 51.5074,
  lng: -0.1278
};

const markers = [
  {
    position: { lat: 51.5074, lng: -0.1278 },
    label: 'Londres'
  },
  {
    position: { lat: 48.8566, lng: 2.3522 },
    label: 'Paris'
  }
  // Ajoutez plus de marqueurs selon vos besoins
];

function Map() {
  const [activeMarker, setActiveMarker] = useState(null);

  const handleMarkerClick = (marker) => {
    setActiveMarker(marker);
  };

  const handleCloseClick = () => {
    setActiveMarker(null);
  };

  return (
    <div className="map-container">
      <GoogleMap
        mapContainerStyle={containerStyle}
        center={center}
        zoom={5}
      >
        {markers.map((marker, index) => (
          <Marker
            key={index}
            position={marker.position}
            label={marker.label}
            onClick={() => handleMarkerClick(marker)}
          />
        ))}

        {activeMarker && (
          <InfoWindow
            position={activeMarker.position}
            onCloseClick={handleCloseClick}
          >
            <div>
              <h3>{activeMarker.label}</h3>
              <p>Informations supplémentaires</p>
            </div>
          </InfoWindow>
        )}
      </GoogleMap>
    </div>
  );
}

export default Map;
